import dto.Benefit;
import dto.ChartData;
import dto.Employee;
import dto.PieChart;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PdfReportGenerator {

    public void generatePdf(List<Employee> employees, List<Benefit> benefits, List<ChartData> chartData, List<PieChart> pieData) throws FileNotFoundException, JRException {
        String outputFile = "D:\\Users\\Sitdikov-RN\\JaspersoftWorkspace\\MyJasperProject\\" + "JasperReportExample.pdf";
        InputStream input = new FileInputStream(new File("D:\\Users\\Sitdikov-RN\\JaspersoftWorkspace\\MyJasperProject\\JasperReport_A4_3.jrxml"));

        JRBeanCollectionDataSource pdfTableParameters = new JRBeanCollectionDataSource(employees);
        JRBeanCollectionDataSource benefitTableParameter = new JRBeanCollectionDataSource(benefits);
        JRBeanCollectionDataSource dataChartParameter = new JRBeanCollectionDataSource(chartData);
        JRBeanCollectionDataSource pieChartParameter = new JRBeanCollectionDataSource(pieData);

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("EmployeeTableParameters", pdfTableParameters);
        parameters.put("BenefitTableParameter", benefitTableParameter);
        parameters.put("ChartParameter", dataChartParameter);
        parameters.put("PieChartParameter", pieChartParameter);

        JasperDesign jasperDesign = JRXmlLoader.load(input);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());

        JasperViewer.viewReport(jasperPrint);
        OutputStream outputStream = new FileOutputStream(new File(outputFile));
        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
        System.out.println("OK");
    }
}