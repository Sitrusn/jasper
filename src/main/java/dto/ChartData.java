package dto;

public class ChartData {
    private String series;
    private String category;
    private double value;

    public ChartData(String series, String category, double value) {
        super();
        this.series = series;
        this.category = category;
        this.value = value;
    }

    public String getSeries() {
        return series;
    }

    public String getCategory() {
        return category;
    }

    public double getValue() {
        return value;
    }
}
