package dto;

public class PieChart {
    private Integer pieValue;
    private String country;

    public PieChart(Integer pieValue, String country) {
        this.pieValue = pieValue;
        this.country = country;
    }

    public Integer getPieValue() {
        return pieValue;
    }

    public String getCountry() {
        return country;
    }
}
