package dto;

public class Benefit {
    private Integer firstBar;
    private Integer secondBar;

    public Benefit(Integer firstBar, Integer secondBar) {
        this.firstBar = firstBar;
        this.secondBar = secondBar;
    }

    public Integer getFirstBar() {
        return firstBar;
    }

    public Integer getSecondBar() {
        return secondBar;
    }
}
