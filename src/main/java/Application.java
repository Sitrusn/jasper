import dto.Benefit;
import dto.ChartData;
import dto.Employee;
import dto.PieChart;
import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) throws FileNotFoundException, JRException {

        List employeesTable = new ArrayList<Employee>();
        Employee employee1 = new Employee(1, "Имя #1", "Отчество #1", "Адрес проживания #1");
        Employee employee2 = new Employee(2, "Имя #2", "Отчество #2", "Адрес проживания #2");
        employeesTable.add(employee1);
        employeesTable.add(employee2);

        List benefitTable = new ArrayList<Employee>();
        Benefit benefits = new Benefit(10, 17);
        benefitTable.add(benefits);

        List<ChartData> chartList = new ArrayList<ChartData>();
        chartList.add(new ChartData("Выгода в %","Вклад #1", 10));
        chartList.add(new ChartData("Выгода в %","Вклад #2", 17));

        List pieList = new ArrayList<Employee>();
        PieChart pieChart = new PieChart(21, "country");
        pieList.add(pieChart);



        new PdfReportGenerator().generatePdf(employeesTable, benefitTable, chartList, pieList);
    }
}

